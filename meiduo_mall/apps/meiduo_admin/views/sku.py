from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from apps.goods.models import SKU, SPUSpecification
from apps.meiduo_admin.serializers.sku import SKUSerializer, GoodsCategorySerializer, SPUSerializer, \
    SPUSpecificationSerializer
from apps.meiduo_admin.utils import PageNum


class SKUModelViewSet(ModelViewSet):

    # queryset = SKU.objects.all()
    def get_queryset(self):

        keyword=self.request.query_params.get('keyword')

        if keyword:
            return SKU.objects.filter(name__contains=keyword)

        return SKU.objects.all()

    serializer_class = SKUSerializer

    pagination_class = PageNum

"""
获取三级分类数据
GoodsCategory
GoodsCategory.objects.filter(parent_id__gt=37)
GoodsCategory.objects.filter(subs=None)
"""
from apps.goods.models import GoodsCategory
from rest_framework.generics import ListAPIView
class ThreeCategoryListAPIView(ListAPIView):

    queryset = GoodsCategory.objects.filter(parent_id__gt=37)

    serializer_class = GoodsCategorySerializer

"""
获取所有的SPU的数据
"""
from apps.goods.models import SPU
class SPUListAPIView(ListAPIView):

    queryset = SPU.objects.all()

    serializer_class = SPUSerializer


"""
需求:
    当我们选择了某一个SPU之后(iPhone8 Plus),
    先获取商品所对应的 规格(颜色,内存),
    确定了规格之后,我们再根据规格获取规格选项信息( 颜色:金灰银)


大体步骤:
    1.获取spu的id
    2.根据spu来获取 spu的规格信息
    3.根据规格信息,获取规格选项信息

请求方式:
    GET     meiduo_admin/goods/(?P<pk>\d+)/specs/

"""

# class SPUSpecsAPIView(APIView):
#
#     def get(self,request,pk):
#         # 1.获取spu的pk
#         # 2.根据spu来获取 spu的规格信息
#         # 3.根据规格信息,获取规格选项信息
#         specs=SPUSpecification.objects.filter(spu_id=pk)
#         # specs = [SPUSpecification,SPUSpecification,...]
#         s = SPUSpecificationSerializer(specs,many=True)
#
#         return Response(s.data)

from rest_framework.generics import ListAPIView,RetrieveAPIView
class SPUSpecsAPIView(ListAPIView):

    # queryset = SPUSpecification.objects.filter(spu_id=pk)
    def get_queryset(self):
        pk=self.kwargs.get('pk')
        return SPUSpecification.objects.filter(spu_id=pk)


    serializer_class = SPUSpecificationSerializer

