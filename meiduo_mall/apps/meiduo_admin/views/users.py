
from rest_framework.mixins import ListModelMixin
from rest_framework.generics import ListAPIView

from apps.meiduo_admin.serializers.users import UserSerializer,UserAddSerializer
from apps.meiduo_admin.utils import PageNum
from apps.users.models import User

"""
需求(明确你要做什么):
    把所有的用户罗列出来
大体的步骤:

    1.先实现获取所有用户
        1.1 查询所有的用户 [User.User,User,...]
        1.2 对象列表转换为字典列表
        1.3 返回数据

    2.再实现分页


    3.最后实现搜索
请求方式和路由
    GET     users
视图


"""
from rest_framework.views import APIView
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import CreateModelMixin
from rest_framework.generics import ListCreateAPIView
class UsersListAPIView(ListCreateAPIView):
    # queryset = User.objects.all()
    def get_queryset(self):
        keyword=self.request.query_params.get('keyword')

        if keyword:
            return User.objects.filter(username__contains=keyword)

        return User.objects.all()
    """
    如果你只使用一个序列化器可以设置属性
    如果你需要使用多个序列化器则使用 方法
    """
    # serializer_class = UserSerializer
    # serializer_class = UserAddSerializer
    def get_serializer_class(self):

        if self.request.method == 'GET':
            return UserSerializer
        else:
            return UserAddSerializer
    pagination_class = PageNum
