from rest_framework.generics import ListAPIView
from rest_framework.viewsets import ModelViewSet
from apps.meiduo_admin.serializers.group import GroupSerializer
from apps.meiduo_admin.utils import PageNum
from django.contrib.auth.models import Group,Permission

class GroupModelViewSet(ModelViewSet):

    queryset = Group.objects.all()

    serializer_class = GroupSerializer

    pagination_class = PageNum

from apps.meiduo_admin.serializers.group import PermissionSerializer
class PermisssionAllListAPIView(ListAPIView):

    queryset = Permission.objects.all()

    serializer_class = PermissionSerializer