from django.contrib.auth.models import Group
from rest_framework.generics import ListAPIView
from rest_framework.viewsets import ModelViewSet

from apps.meiduo_admin.serializers.admin import UserSerializer
from apps.meiduo_admin.serializers.group import GroupSerializer
from apps.meiduo_admin.utils import PageNum
from apps.users.models import User


class AdminModelViewSet(ModelViewSet):

    queryset = User.objects.filter(is_staff=True)

    serializer_class = UserSerializer

    pagination_class = PageNum


    # 在这里重写是可以的,但是重写的代码多
    # def create(self, request, *args, **kwargs):
    #
    #     pass

class GroupAllListAPIView(ListAPIView):

    queryset = Group.objects.all()

    serializer_class = GroupSerializer


