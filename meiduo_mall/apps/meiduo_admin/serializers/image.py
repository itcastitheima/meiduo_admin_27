from rest_framework import serializers
from apps.goods.models import SKUImage, SKU


class SKUImageSerializer(serializers.ModelSerializer):

    sku=serializers.PrimaryKeyRelatedField(read_only=True)
    #或
    # sku=serializers.PrimaryKeyRelatedField(queryset=SKU.objects.all())

    class Meta:
        model = SKUImage
        fields=['id','sku','image']

#获取新增图片的SKU数据 序列化器
class SimpleSKUSerializer(serializers.ModelSerializer):

    class Meta:
        model = SKU
        fields = ['id','name']