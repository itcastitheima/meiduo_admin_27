from rest_framework import serializers

from apps.users.models import User


# 用户的展示
class UserSerializer(serializers.ModelSerializer):


    class Meta:
        model = User
        fields=['id','username','mobile','email']


# 用户的新增
class UserAddSerializer(serializers.ModelSerializer):

    password2=serializers.CharField(write_only=True)


    class Meta:
        model = User
        """
         "username": this.userForm.username,
          "mobile": this.userForm.mobile,
          "password": this.userForm.password,
          "email": this.userForm.email
        """
        fields = ['username','password','mobile','email','password2']
        extra_kwargs = {
            'password':{
                # 只在反序列化的时候 使用
                # 序列化(对象-->字典)的时候忽略此字段
                'write_only':True
            }
        }

    def validate(self, attrs):
        password=attrs.get('password')
        password2=attrs.get('password2')

        if password != password2:
            raise serializers.ValidationError('密码不一致')
        #要有返回值
        return attrs



    def create(self, validated_data):
        #删除多余数据
        del validated_data['password2']

        user = User.objects.create_user(**validated_data)
        return user
