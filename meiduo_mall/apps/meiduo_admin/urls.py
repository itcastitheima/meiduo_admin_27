from django.conf.urls import url

#导入 jwt的视图
from rest_framework_jwt.views import obtain_jwt_token

from .views import statistical
from .views import users
from .views import image
from .views import sku
from .views import order
from .views import permission
from .views import group
from .views import admin
#########################图片路由#########################################
#图片的管理视图集的url
urlpatterns = [

    url(r'^authorizations/$', obtain_jwt_token),

    ######################统计###############################
    url(r'^statistical/total_count/$', statistical.UserAllCountAPIView.as_view()),

    url(r'^statistical/day_increment/$', statistical.UserDayAddCountAPIView.as_view()),


    url(r'^statistical/day_active/$', statistical.UserDayActiveCountAPIView.as_view()),


    url(r'^statistical/day_orders/$', statistical.UserDayOrdersCountAPIView.as_view()),

    url(r'^statistical/month_increment/$', statistical.UserMonthCountAPIView.as_view()),

    url(r'^statistical/goods_day_views/$', statistical.GoodsDayViewCountAPIView.as_view()),


    ########################User相关###################################
    url(r'^users/$', users.UsersListAPIView.as_view()),

    ####################图片相关的###########################

    url(r'^skus/simple/$', image.SimpleSKUListAPIView.as_view()),


    ##################sku相关##########################################
    url(r'^skus/categories/$', sku.ThreeCategoryListAPIView.as_view()),
    url(r'^goods/simple/$', sku.SPUListAPIView.as_view()),

    #获取规格信息的路由
    url(r'^goods/(?P<pk>\d+)/specs/$', sku.SPUSpecsAPIView.as_view()),


    ########################权限#######################################
    url(r'^permission/content_types/$', permission.ContentTypeAPIView.as_view()),

    url(r'^permission/simple/$', group.PermisssionAllListAPIView.as_view()),

    #分组
    url(r'^permission/groups/simple/$', admin.GroupAllListAPIView.as_view()),


    #url(r'^authorizations/$',views.AdminLoginAPIView.as_view()),
]
from rest_framework.routers import DefaultRouter

#创建router实例对象
router = DefaultRouter()
#注册路由
router.register(r'skus/images',image.ImageModelViewSet,basename='images')
#将路由追加到urlpatterns
urlpatterns += router.urls


#############################SKU路由#######################################
router = DefaultRouter()

router.register(r'skus',sku.SKUModelViewSet,basename='skus')

urlpatterns += router.urls

#############################订单路由#######################################
router = DefaultRouter()

router.register(r'orders',order.OrderModelViewSet,basename='orders')

urlpatterns += router.urls

#############################权限路由#######################################
router = DefaultRouter()

router.register(r'permission/perms',permission.PermissionModelViewSet,basename='perms')

urlpatterns += router.urls

#############################组路由#######################################
router = DefaultRouter()

router.register(r'permission/groups',group.GroupModelViewSet,basename='group')

urlpatterns += router.urls

#############################管理员路由#######################################
router = DefaultRouter()

router.register(r'permission/admins',admin.AdminModelViewSet,basename='admin')

urlpatterns += router.urls

"""
给普通(消费者)用户: 前台
给管理员:          后台



老师(n) 与 学生(m)

table_teacher

id      name
1        刘老师
2        齐老师

table_student

id      name
111      张三
222       李四


tea_id      stu_id

1               111
1               222
2               111
2               222

"""
